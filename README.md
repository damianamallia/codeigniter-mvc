# README

## Prerequisites

This website has been tested using:

* Apache 2.0
* PHP 5.3.15
* MySQL 14.14
* Google Chrome 29.0.1547.65

## Database Configuration

The project uses the `ci_mvc` database. The database is automatically created
if it does not exist. It is possible to specify DB connection settings (e.g. username,
password, host) by editing the following file:

```
/codeigniter-mvc/config/database.php
```

## Libraries

The following libraries have been used in the project.

* CodeIgniter PHP framework
* Twitter Bootstrap
* jQuery
* jQueryForm

## CodeIgniter and MVC

The project is built using the CodeIgniter PHP framework, which makes
extensive use of the Model-View-Controller (MVC) pattern. All of the models,
views and controllers are contained in their respective directories:

```
/codeigniter-mvc/application/models
/codeigniter-mvc/application/views
/codeigniter-mvc/application/controllers
```

### Models

The `m_common` model is responsible for initializing the database.
Ideally, this should be moved into a separate model, only used during the
installation phase.

The `m_programs` model is responsible for interacting with the database.
Functions to insert, extract and delete items have been implemented.

### Views

Views are responsible for visualizing information obtained via the models.
They are all contained in `/codeigniter-mvc/application/views`.
All the common parts, such as the HTML headers and the navigation bar, are
contained inside a common view named `v_common`.

### Controllers

Controllers contain the application logic. They are all contained in
`/codeigniter-mvc/application/controllers`. In this implementation, controllers are also
responsible for input validation.

## Input Validation

Both server-side and client-side validation have been implemented.
While server-side validation provides a good degree of security, client-side
validation improves the user experience.
XML files are only validate for syntactical correctness and not against a
schema. This could be added in a future version of the project.

## Helpers

To simplify XML handling, a `MY_xml_helper` helper has been created.
The helper extends the already existing `xml_helper`.

## Uploaded Files

Imported XML files are temporarily stored in a `uploads` directory.
Ensure your web server user has write access to that folder.

## CSS and JS

Application-specific CSS and JS files are contained in `/codeigniter-mvc/static`

* css/app.css
* js/app.js
