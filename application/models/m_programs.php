<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * M_programs model
 *
 * @package codeigniter_mvc
 * @subpackage Models
 * @author Damiana Mallia
 **/
class M_programs extends M_common 
{	
    function __construct()
    {
        parent::__construct();
    }
	
	/**
	 * insert
	 *
	 * @author Damiana Mallia
	 **/
	public function insert($data)
	{
		foreach($data as $key => $value)
		{
			$data[$key] = $this->security->xss_clean($value);
		}
		$exploded_date  = explode('+', $data['date']);
		$data['date']     = $exploded_date[0];
		$data['timezone'] = '+' . (!empty($exploded_date[1]) ? $exploded_date[1] : '00:00');
		
		return $this->db->insert($this->table, $data); 
	}
	
	/**
	 * get_data
	 *
	 * Execute a select query and return the obtained results as an array.
	 * @author Damiana Mallia
	 **/
	public function get_data()
	{
		return $this->db->select("*, CONCAT(DATE_FORMAT(date, '%Y-%m-%dT%T'), timezone) as date", FALSE)
			     ->from($this->table)
				 ->order_by("name")
				 ->get()->result();
	}
	
	/**
	 * get_data_query
     * Return a select query handler. Used by the XML export.
	 *
	 * @author Damiana Mallia
	 **/
	public function get_data_query()
	{
		return $this->db->select("
			CONCAT(DATE_FORMAT(date, '%Y-%m-%dT%T'), timezone) as date,
			start_time,
			leadtext,
			name,
			`b-line`,
			synopsis,
			url
			", FALSE)
			     ->from($this->table)
				 ->order_by("name")
				 ->get();
	}
	
	/**
	 * remove
	 *
	 * Remove an entry from the database.
	 * @author Damiana Mallia
	 **/
	public function remove($id)
	{
		return $this->db->delete($this->table, array('id' => $id)); 
	}
}
/* End of file m_programs.php */
/* Location: ./application/models/m_programs.php */