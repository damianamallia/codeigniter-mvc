<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Common model
 *
 * @package codeigniter-mvc
 * @subpackage Models
 * @author Damiana Mallia
 **/
class M_common extends CI_Model 
{	
	public $db_name = 'ci_mvc';
	public $table = 'program';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
		$this->init_db();
    }

	/**
	 * init_db
	 *
	 * @author Damiana Mallia
	 **/
	public function init_db()
	{
		$this->load->dbforge();
		$this->load->dbutil();
		
		// Create the database
		if (! $this->dbutil->database_exists($this->db_name))
		{
		   $this->dbforge->create_database($this->db_name);
		}

		//Create fields of 'program' table
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 5, 
				'unsigned' => TRUE,
				'auto_increment' => TRUE
				),
			'date' => array(
				'type' => 'datetime'
				),
			'timezone' => array(
				'type' => 'VARCHAR',
				'constraint' => 6
				),
			'start_time' => array(
				'type' =>'time'
				),
			'leadtext' => array(
				'type' => 'TEXT'
				),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => 100 
				),
			'b-line' => array(
				'type' => 'VARCHAR',
				'constraint' => 100
				),
			'synopsis' => array(
				'type' => 'mediumtext'
				),
			'url' => array(
				'type' => 'TEXT'
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		//Create 'program' table
		$this->dbforge->create_table($this->table, TRUE);
	}
}
/* End of file m_common.php */
/* Location: ./application/models/m_common.php */