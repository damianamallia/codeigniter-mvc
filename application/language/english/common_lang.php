<?php
$lang['title']          = "Programs";
$lang['date']           = "Date";
$lang['start_time']     = "Start Time";
$lang['leadtext']       = "Lead Text";
$lang['name']           = "Name";
$lang['b-line']         = "B-Line";
$lang['synopsis']       = "Synopsis";
$lang['url']            = "URL";
$lang['submit']         = "Submit";
$lang['upload']         = "Upload";
$lang['help_import']    = "Import an XML file to add the data to the database.";
$lang['bad_xml']        = "The provided XML file is not valid.";
$lang['bad_insert']     = "The provided entry is not valid.";
?>