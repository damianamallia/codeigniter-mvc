<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Common controller
 *
 * @package codeigniter-mvc
 * @subpackage Controllers
 * @author Damiana Mallia
 **/
class Common extends CI_Controller 
{
	public $html;
	public $data;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_common');
		$this->lang->load('common', 'english');
	}
	
	/**
	 * Index
	 *
	 * @author Damiana Mallia
	 **/
	public function index()
	{
		$this->_load_template();
	}
	
	/**
	 * Load template
	 *
	 * @author Damiana Mallia
	 **/
	protected function _load_template()
	{
		return $this->load->view('v_common', $this->html);
	}
}

/* End of file controller.php */
/* Location: ./application/controllers/controller.php */