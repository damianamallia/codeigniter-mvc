<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Upload controller
 *
 * @package codeigniter-mvc
 * @subpackage Controllers
 * @author Damiana Mallia
 **/

require "common.php";

class Upload extends Common {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('m_programs');
		$this->html->page = 'import';
	}

	function index()
	{
		$this->html->content = $this->load->view('v_upload', array('error' => ' ' ), true);
		$this->_load_template();
	}

	function do_upload()
	{
		// Intial HTTP Status Code.
		// 200 -> OK
		$status_code = '200';

		$config['upload_path'] = './uploads/';
		$config['file_type'] = 'txt|xml';
		$config['allowed_types'] = 'xml';

		$this->load->library('upload', $config);
		$this->load->helper('xml');
		$this->load->helper('file');
	
		if ($this->upload->do_upload())
		{
			// File has successfully been uploaded.
			// Validate XML.
			$data = array('upload_data' => $this->upload->data());
			$full_path = $data['upload_data']['full_path'];
			$xml = $this->get_validated_xml($full_path);
			if($xml)
			{
				// Validation passed.
				// Insert data into database.
				foreach ($xml->program as $program)
				{
					$array = xml2array($program);
					$this->m_programs->insert($array);
				}
			}
			else
			{
				// Validation failed.
				// Set proper HTTP response and status code.
				// 400 -> Malformed Request.
				$error = array('error' => lang('bad_xml'));
				$status_code = '400';
				$this->output->set_output(json_encode($error));
			}
			// No need to store the file, after it has been imported.
			unlink($full_path);
		}
		else
		{
			// Upload not completed.
			// Set proper HTTP response and status code.
			// 400 -> Malformed Request.
			$error = array('error' => $this->upload->display_errors());
			$status_code = '400';
			$this->output->set_output(json_encode($error));
		}
		// Set the HTTP status code of the response.
		$this->output->set_status_header($status_code);
	}
	
	/**
	 * Validate xml file
	 * Returns an object of class SimpleXMLElement, or FALSE on failure.
	 * 
	 * @author Damiana Mallia
	 **/
	private function get_validated_xml($filename)
	{
		//Enable user error handling
		libxml_use_internal_errors(true);

		$xmlstr = file_get_contents($filename);
		$doc = simplexml_load_string($xmlstr);
		
		return $doc;
	}
}
?>