<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Programs controller
 *
 * @package codeigniter-mvc
 * @subpackage Controllers
 * @author Damiana Mallia
 **/

require "common.php";

class Programs extends Common 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_programs');
	}
	
	/**
	 * Index
	 *
	 * @author Damiana Mallia
	 **/
	public function index()
	{
		$this->html->page = 'list';
		$this->data->programs = $this->m_programs->get_data();
		$this->html->content = $this->load->view('v_list', $this->data, true);

		$this->_load_template();
	}
	
	/**
	 * Insert
	 *
	 * @author Damiana Mallia
	 **/
	public function insert()
	{
		$this->html->page = 'insert';
		$terms = $this->input->post();

		if($terms)
		{
			// Dealing with a POST request.
			// Validate the input from the user.
			$this->load->library('form_validation');
			$this->form_validation->set_rules('date', 'Date', 'required');
			$this->form_validation->set_rules('start_time', 'start_time', 'required');
			$this->form_validation->set_rules('leadtext', 'Leadtext', 'required');
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('b-line', 'Name', 'required');
			$this->form_validation->set_rules('synopsis', 'Name', 'required');
			$this->form_validation->set_rules('url', 'Name', 'required');

			if ($this->form_validation->run())
			{
				// Validation passed. Insert into the database.
				$this->m_programs->insert($terms);
			}
			else
			{
				// Validation failed. Set proper HTTP response and status code.
				// 400 -> Malformed request.
				$error = array('error' => lang('bad_insert'));
				$this->output->set_output(json_encode($error));
				$this->output->set_status_header('400');
			}
		}
		else
		{
			// Dealing with a GET request.
			// Show the HTML form.
			$this->html->content = $this->load->view('v_insert', $this->data, true);
			$this->_load_template();
		}
	}
	
	/**
	 * Delete
	 *
	 * @author Damiana Mallia
	 **/
	public function delete($id)
	{
		$this->m_programs->remove($id);
		redirect("programs");
	}
	
	/**
	 * Export xml
	 *
	 * @author Damiana Mallia
	 **/
	public function export_xml()
	{
		$this->html->page = 'export';
		$this->load->dbutil();
		$this->load->helper('download');
	
		// Use a library function to export data as XML.
		$query = $this->m_programs->get_data_query();
		$config = array (
		                  'root'    => 'programs',
		                  'element' => 'program', 
		                  'newline' => "\n", 
		                  'tab'    => "\t"
		                );
		$data = $this->dbutil->xml_from_result($query, $config);
		// Set the XML header
		$header = '<?xml version="1.0"?>';
		$name = 'programs.xml';
		// Instruct the browser to download the file.
		force_download($name, $header . $data);
	}
}

/* End of file programs.php */
/* Location: ./application/controllers/programs.php */