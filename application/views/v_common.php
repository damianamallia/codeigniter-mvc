<!-- v_common -->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?=lang('title')?></title>
	<link rel="stylesheet" type="text/css" href="<?=base_url('static/css/bootstrap.min.css')?>"/>
	<link rel="stylesheet" type="text/css" href="<?=base_url('static/css/app.css')?>"/>
</head>
<body>

	<nav class="navbar navbar-default" role="navigation">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">Code Igniter MVC</a>
		</div>
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul id="nav" class="nav navbar-nav">
				<li id="list"><a href="<?=base_url('index.php/programs')?>">List</a></li>
				<li id="insert"><a href="<?=base_url('index.php/programs/insert')?>">Insert</a></li>
				<li id="import"><a href="<?=base_url('index.php/upload')?>">Import</a></li>
				<li id="export"><a href="<?=base_url('index.php/programs/export_xml')?>">Export</a></li>
			</ul>
		</div>
	</nav>

	<div id="container" class="ci_mvc70">
		<div id="alert" class="alert"></div>
		<?=$content?>
	</div>

<div id="current_page" data="<?=$page ?>"></div>

<script src="<?=base_url('static/js/jquery.js')?>"></script>
<script src="<?=base_url('static/js/jquery.form.min.js')?>"></script>
<script src="<?=base_url('static/js/bootstrap.min.js')?>"></script>
<script src="<?=base_url('static/js/app.js')?>"></script>
</body>
</html>
<!-- END: v_common -->