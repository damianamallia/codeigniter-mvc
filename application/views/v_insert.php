<!-- v_insert -->
<div>
	<form id="insert_form" action="insert" method="post" role="form">
		<div class="form-group">
			<label for="date"><?=lang('date')?></label>
			<input type="datetime-local" name="date" class="form-control" id="date" required>
		</div>
		<div class="form-group">
			<label for="start_time"><?=lang('start_time')?></label>
			<input type="time" name="start_time" class="form-control" id="start_time" required>
		</div>
		<div class="form-group">
			<label for="leadtext"><?=lang('leadtext')?></label>
			<input type="text" name="leadtext" class="form-control" id="leadtext" required>
		</div>
		<div class="form-group">
			<label for="name"><?=lang('name')?></label>
			<input type="text" name="name" class="form-control" id="name" required>
		</div>
		<div class="form-group">
			<label for="b-line"><?=lang('b-line')?></label>
			<input type="text" name="b-line" class="form-control" id="b-line" required>
		</div>
		<div class="form-group">
			<label for="synopsis"><?=lang('synopsis')?></label>
			<input type="text" name="synopsis" class="form-control" id="synopsis" required>
		</div>
		<div class="form-group">
			<label for="url"><?=lang('url')?></label>
			<input type="text" name="url" class="form-control" id="url" required>
		</div>
		<button type="submit" class="btn btn-default"><?=lang('submit')?></button>
	</form>
</div>
<!-- END: v_insert -->