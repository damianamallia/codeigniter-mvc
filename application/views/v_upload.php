<!-- v_upload -->
<form role="form" action="upload/do_upload" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label for="userfile">File input</label>
		<input type="file" id="userfile" name="userfile">
		<p class="help-block"><?=lang('help_import')?></p>
	</div>
	<button type="submit" class="btn btn-default"><?=lang('upload')?></button>
</form>
<!-- END: v_upload -->