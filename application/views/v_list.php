<!-- v_list -->
<div>
	<?if(!empty($programs)):?>
	<table class="table table-striped">
		<tr>
			<th>Date</th>
			<th>Start time</th>
			<th>leadtext</th>
			<th>name</th>
			<th>b-line</th>
			<th>synopsis</th>
			<th>url</th>
			<th>Delete</th>
		</tr>
		<? foreach($programs as $program): ?>
		<tr>
			<td><?=$program->date?></td>
			<td><?=$program->start_time?></td>
			<td><?=$program->leadtext?></td>
			<td><?=$program->name?></td>
			<td><?=$program->{'b-line'}?></td>
			<td><?=$program->synopsis?></td>
			<td><?=$program->url?></td>
			<td><a href="programs/delete/<?=$program->id?>">Delete</a></td>
		</tr>
	 	<? endforeach ?>
	</table>
	<?else:?>
		No programs, yet.
	<?endif?>
</div>
<!-- END: v_list -->