$(document).ready(function(){
	// When document is ready...
	
	// ...highlight the current page in the menu.
	var current_page = $("div#current_page").attr("data");
	$("#nav li").removeClass("active");
	$("#nav #" + current_page).addClass("active");

	// ... Set the forms to use Ajax.
	$('form').ajaxForm({
		success: success,
		error: error
	});
});

function success(){
	// Display a success message.
	$('#alert').html('Operation successfully completed!');
	$('#alert').removeClass('alert-danger');
	$('#alert').addClass('alert-success');
	$('#alert').fadeIn().delay(2000).fadeOut();	
}

function error(data){
	// Display an error message.
	$error_msg = $.parseJSON(data.responseText).error;
	$('#alert').html($error_msg);
	$('#alert').removeClass('alert-success');
	$('#alert').addClass('alert-danger');
	$('#alert').fadeIn().delay(2000).fadeOut();	
}
